//require('dotenv').config();
const mongoose = require("mongoose");

//2const { NOTES_APP_MONGODB_HOST, NOTES_APP_MONGODB_DATABASE } = process.env;

//1const MONGODB_URI = 'mongodb://localhost:4001:/notasDBMongo';
//2const MONGODB_URI = `mongodb://${NOTES_APP_MONGODB_HOST}/${NOTES_APP_MONGODB_DATABASE}`;
const MONGODB_URI = 'mongodb+srv://jose:jose987@cluster0.mmvte.mongodb.net/notasDBMongo?retryWrites=true&w=majority';


mongoose
  .connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true 
  })
  .then(db => console.log("DB is connected"))
  .catch(err => console.error(err));
